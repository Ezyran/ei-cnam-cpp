#ifndef TICTACTOEGAME_H
#define TICTACTOEGAME_H
#include "tictactoegrid.h"
#include "combinatorygame.h"

class TicTacToeGame : public CombinatoryGame
{
public:
    TicTacToeGame(TicTacToeGrid &grid);
    void PlayTurn();
    bool IsOver();

protected:
    TicTacToeGrid &grid;
    Cell& HandlePlayerInput();
};

#endif // TICTACTOEGAME_H
