#ifndef CELLS_H
#define CELLS_H

#include <vector>
#include <functional>
#include "cell.h"
#include "player.h"

class Cells
{
public:
    Cells();
    bool IsOwnedBy(const Player player);
    void AddCell(Cell& cell);
    std::vector<std::reference_wrapper<Cell>> GetCells();
private:
    // Ensemble de références de cases
    std::vector<std::reference_wrapper<Cell>> cells = std::vector<std::reference_wrapper<Cell>>();
};

#endif // CELLS_H
