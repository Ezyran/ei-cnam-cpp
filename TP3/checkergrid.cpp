#include <iostream>
#include "checkergrid.h"

CheckerGrid::CheckerGrid() : Grid(10,10)
{

}

void CheckerGrid::MoveCell(Cell& pawn, Cell& destination)
{
    int tempLine = pawn.line;
    int tempCol = pawn.column;

    pawn.line = destination.line;
    pawn.column = destination.column;

    destination.line = tempLine;
    destination.column = tempCol;
    std::cout << "" << std::endl;
}

void CheckerGrid::ClearCell(Cell& cell)
{
    cell.value = "0";
}
