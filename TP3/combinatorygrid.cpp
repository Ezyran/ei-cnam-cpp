#include "combinatorygrid.h"

CombinatoryGrid::CombinatoryGrid(int length, int width) : Grid(length, width)
{
    this->GenerateLines();
    this->GenerateColumns();
}

std::vector<Cells> CombinatoryGrid::GetLines()
{
    return this->lines;
}

std::vector<Cells> CombinatoryGrid::GetColumns()
{
    return this->columns;
}

std::vector<Cells> CombinatoryGrid::GetDiagonals()
{
    return this->diagonals;
}

bool CombinatoryGrid::IsFull()
{
    int numberOfCells = static_cast<int>(this->cells.size());
    for (int i = 0; i < numberOfCells; i++)
    {
        if (this->cells.at(i).value == "0")
            return false;
    }
    return true;
}

void CombinatoryGrid::GenerateLines()
{
    for (int i = 0; i < this->length; i++)
    {
        Cells line = Cells();
        for (int j = 0; j < this->width; j++)
        {
            line.AddCell(this->GetCellFromCoords(i, j));
        }
        this->lines.push_back(line);
    }
}

void CombinatoryGrid::GenerateColumns()
{
    for (int i = 0; i < this->width; i++)
    {
        Cells column = Cells();
        for (int j = 0; j < this->length; j++)
        {
            column.AddCell(this->GetCellFromCoords(j, i));
        }
        this->columns.push_back(column);
    }
}

bool CombinatoryGrid::IsColumnOwnedByPlayer(int columnNum, const Player& player)
{
    return this->columns.at(columnNum).IsOwnedBy(player);
}

bool CombinatoryGrid::IsDiagonalOwnedByPlayer(int diagonalNum, const Player& player)
{
    return this->diagonals.at(diagonalNum).IsOwnedBy(player);
}
