#include "cells.h"

Cells::Cells()
{

}

bool Cells::IsOwnedBy(const Player player)
{
    int numberOfCells = static_cast<int>(this->cells.size());
    for (int i = 0; i < numberOfCells; i++)
    {
        Cell cell = this->cells[i].get();
        if (cell.value != player.value)
            return false;
    }
    return true;
}

void Cells::AddCell(Cell& cell)
{
    this->cells.push_back(cell);
}

std::vector<std::reference_wrapper<Cell>> Cells::GetCells()
{
    return std::vector<std::reference_wrapper<Cell>>(this->cells);
}
