TEMPLATE = app
CONFIG += console c++15
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        cell.cpp \
        cells.cpp \
        checkergame.cpp \
        checkergrid.cpp \
        combinatorygame.cpp \
        combinatorygrid.cpp \
        connectfourgame.cpp \
        connectfourgrid.cpp \
        game.cpp \
        grid.cpp \
        main.cpp \
        tictactoegame.cpp \
        tictactoegrid.cpp

HEADERS += \
    cell.h \
    cells.h \
    checkergame.h \
    checkergrid.h \
    checkerplayer.h \
    combinatorygame.h \
    combinatorygrid.h \
    connectfourgame.h \
    connectfourgrid.h \
    game.h \
    grid.h \
    player.h \
    tictactoegame.h \
    tictactoegrid.h
