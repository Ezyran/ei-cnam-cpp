#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "grid.h"

class Game
{
public:
    Game(Grid& grid);
    virtual void InitGame();
    virtual void PlayTurn() = 0;
    virtual bool IsOver() = 0;

protected:
    Player players[2];
    Grid &grid;
    bool isOver;
    virtual Cell& HandlePlayerInput() = 0;
    virtual bool IsWonByPlayer(const Player &player) = 0;
};

#endif // GAME_H
