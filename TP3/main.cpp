#include <iostream>
#include <vector>
#include <memory>
#include "checkergame.h"
#include "checkergrid.h"

int main()
{
    CheckerGrid grid = CheckerGrid();

    std::unique_ptr<Game> game(new CheckerGame(grid));

    game->InitGame();

    while (!game->IsOver())
    {
        game->PlayTurn();
    }
}
