#ifndef GRID_H
#define GRID_H

#include <vector>
#include "cell.h"
#include "cells.h"
#include "player.h"

class Grid
{
public:
    const int length;
    const int width;
    Grid(int length, int width);
    Cell& GetCellFromCoords(int line, int col);
    void Display();

protected:
    std::vector<Cell> cells;
    void GenerateCells();
};

#endif // GRID_H
