#include "combinatorygame.h"

CombinatoryGame::CombinatoryGame(CombinatoryGrid &grid) : Game(grid), grid(grid) {}

bool CombinatoryGame::IsWonByPlayer(const Player &player)
{
    // Vérifie si le joueurs possède une ligne
    int numberOfLines = static_cast<int>(this->grid.GetLines().size());
    for (int i = 0; i < numberOfLines; i++)
    {
        if (this->grid.IsLineOwnedByPlayer(i, player))
            return true;
    }

    // Vérifie si le joueur possède une colonne
    int numberOfColumns = static_cast<int>(this->grid.GetColumns().size());
    for (int i = 0; i < numberOfColumns; i++)
    {
        if (this->grid.IsColumnOwnedByPlayer(i, player))
            return true;
    }

    // Vérifie si le joueur possède une diagonale
    int numberOfDiagonals = static_cast<int>(this->grid.GetDiagonals().size());
    for (int i = 0; i < numberOfDiagonals; i++)
    {
        if (this->grid.IsDiagonalOwnedByPlayer(i, player))
            return true;
    }
    return false;
}
