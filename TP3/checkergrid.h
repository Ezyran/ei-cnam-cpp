#ifndef CHECKERGRID_H
#define CHECKERGRID_H

#include "grid.h"

class CheckerGrid : public Grid
{
public:
    CheckerGrid();
    void MoveCell(Cell& pawn, Cell& destination);
    void ClearCell(Cell& cell);
};

#endif // CHECKERGRID_H
