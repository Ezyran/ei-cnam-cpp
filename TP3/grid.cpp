#include "grid.h"
#include "iostream"

#include <string>

Grid::Grid(int length, int width) : length(length), width(width)
{
    this->GenerateCells();
}

void Grid::Display()
{
    for (int i = 0; i < this->length; i++)
    {
        for (int j = 0; j < this->width; j++)
        {
            std::string cellValue = this->GetCellFromCoords(i, j).value;
            std::cout << " | " << (cellValue == "0" ? " " : cellValue) ;
        }
            std::cout << " | " << std::endl;
    }
}

void Grid::GenerateCells()
{
    for (int i = 0; i < this->length; i++)
    {
        for (int j = 0; j < this->width; j++)
        {
            Cell cell = Cell(i,j);
            this->cells.push_back(cell);
        }
    }
}

Cell& Grid::GetCellFromCoords(int line, int col)
{
    int cellPos = col+line*this->width;
    return this->cells.at(col+line*this->width);
}
