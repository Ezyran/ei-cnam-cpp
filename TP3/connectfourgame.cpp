#include <iostream>
#include "connectfourgame.h"
#include "cell.h"
#include "player.h"

ConnectFourGame::ConnectFourGame(ConnectFourGrid &grid) : CombinatoryGame(grid), grid(grid) {}

void ConnectFourGame::PlayTurn()
{
    for (int i = 0; i < 2; i++)
    {
        Player currentPlayer = this->players[i];
        std::cout << "C'est à " << currentPlayer.name << " de jouer : " << std::endl;

        Cell& cell = this->HandlePlayerInput();
        bool cellIsValid = cell.IsEmpty();
        while (!cellIsValid)
        {
            std::cout << "Il faut désigner une colonne vide !" << std::endl;
            cell = this->HandlePlayerInput();
            cellIsValid = cell.IsEmpty();
        }

        cell.value = currentPlayer.value;

        this->grid.Display();

        if (this->IsWonByPlayer(currentPlayer))
            std::cout << "Félicitations ! " << currentPlayer.name << " a gagné la partie !" << std::endl;

        if (this->grid.IsFull())
            std::cout << "Fin de la partie ! La grille est pleine ..." << std::endl;

        this->isOver = this->IsWonByPlayer(currentPlayer) || this->grid.IsFull();

        if (this->isOver)
            return;
    }
}

bool ConnectFourGame::IsOver()
{
    return this->isOver;
}

Cell &ConnectFourGame::HandlePlayerInput()
{
    int column = 0;
    std::cout << "Saisissez le numéro de colonne de la case à sélectionner : " << std::endl;
    std::cin >> column;
    while (column < 1 || column > grid.width)
    {
        std::cout << "Saisie incorrecte, il faut être dans les bords de la grille. Réessayez :" << std::endl;
        std::cin >> column;
    }

    int line = 0;
    for (int i = 0; i < 4; i++)
    {
        if (grid.GetCellFromCoords(i,column-1).value == "0")
            line = i;
    }

    return grid.GetCellFromCoords(line, column-1);
}
