#include "cell.h"

Cell::Cell(int line, int column)
{
    this->line = line;
    this->column = column;
}

bool Cell::IsOwnedByPlayer(Player &player)
{
    return this->value == player.value;
}

bool Cell::IsEmpty()
{
    return this->value == "0";
}
