#ifndef CELL_H
#define CELL_H

#include "player.h"
#include <string>

class Cell
{
public:
    Cell(int line, int column);

    // Ligne de la case
    int line;

    // Colonne de la case
    int column;

    // Valeur de la case, indique le joueur propriétaire de la case.
    // Vaut 0 lorsque vide.
    std::string value = "0";

    bool IsOwnedByPlayer(Player &player);
    bool IsEmpty();
};

#endif // CELL_H
