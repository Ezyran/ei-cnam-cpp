#ifndef TICTACTOEGRID_H
#define TICTACTOEGRID_H

#include <vector>
#include "player.h"
#include "combinatorygrid.h"

class TicTacToeGrid: public CombinatoryGrid
{
public:
    TicTacToeGrid();
    bool IsLineOwnedByPlayer(int lineNum, const Player& player);

private:
    void GenerateDiagonals();
};

#endif // TICTACTOEGRID_H
