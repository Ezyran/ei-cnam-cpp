#ifndef COMBINATORYGRID_H
#define COMBINATORYGRID_H

#include "grid.h"


class CombinatoryGrid : public Grid
{
public:
    CombinatoryGrid(int length, int width);
    bool IsFull();
    virtual bool IsLineOwnedByPlayer(int lineNum, const Player& player) = 0;
    bool IsColumnOwnedByPlayer(int columnNum, const Player& player);
    bool IsDiagonalOwnedByPlayer(int diagonalNum, const Player& player);

    std::vector<Cells> GetLines();
    std::vector<Cells> GetColumns();
    std::vector<Cells> GetDiagonals();

protected:
    std::vector<Cells> lines;
    std::vector<Cells> columns;
    std::vector<Cells> diagonals;

    void GenerateLines();
    void GenerateColumns();
    virtual void GenerateDiagonals() = 0;
};

#endif // COMBINATORYGRID_H
