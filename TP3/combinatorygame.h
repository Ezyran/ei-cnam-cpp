#ifndef COMBINATORYGAME_H
#define COMBINATORYGAME_H
#include "combinatorygrid.h"
#include "game.h"

class CombinatoryGame : public Game
{
public:
    CombinatoryGame(CombinatoryGrid &grid);
    virtual void PlayTurn() = 0;
    virtual bool IsOver() = 0;

protected:
    CombinatoryGrid &grid;
    bool IsWonByPlayer(const Player &player);
    virtual Cell& HandlePlayerInput() = 0;

};

#endif // COMBINATORYGAME_H
