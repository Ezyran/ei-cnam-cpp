#include <iostream>
#include <cctype>
#include "checkergame.h"

CheckerGame::CheckerGame(CheckerGrid &grid) : Game(grid), grid(grid)
{

}

void CheckerGame::InitGame()
{
    CheckerPlayer blackPlayer;
    blackPlayer.value = "n";
    std::cout << "Saississez le nom du joueur noir : " << std::endl;
    std::cin >> blackPlayer.name;

    CheckerPlayer whitePlayer;
    whitePlayer.value = "b";
    std::cout << "Saississez le nom du joueur blanc : " << std::endl;
    std::cin >> whitePlayer.name;

    this->players[0] = blackPlayer;
    this->players[1] = whitePlayer;

    // Placer des pions noirs
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (i != j && ((i%2 != 0 && j%2 == 0) || (i%2 == 0 && j%2 != 0)))
            {
                Cell& blackCell = this->grid.GetCellFromCoords(i,j);
                blackCell.value = "n";
                 this->players[0].cells.push_back(blackCell);
            }
        }
    }

    // Placer des pions blancs
    for (int i = 6; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (i != j && ((i%2 != 0 && j%2 == 0) || (i%2 == 0 && j%2 != 0)))
            {
                Cell& whiteCell = this->grid.GetCellFromCoords(i,j);
                whiteCell.value = "b";
               this->players[1].cells.push_back(whiteCell);
            }
        }
    }

}

std::vector<Cell> CheckerGame::GetCellNeighbours(Cell &cell, std::string typeValue)
{
    std::vector<Cell> tabCells;
    std::string upperTypeValue = typeValue;
    for (auto &c: upperTypeValue) c = toupper(c);

        if(cell.line - 1 >= 0 && cell.column - 1 >= 0)
        {
            Cell& testCellUpLeft = this->grid.GetCellFromCoords(cell.line - 1, cell.column - 1);
            if( testCellUpLeft.value == typeValue ||  testCellUpLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpLeft);
            }
        }
        if(cell.line - 1 >= 0 && cell.column + 1 <= 9)
        {
            Cell& testCellUpRight = this->grid.GetCellFromCoords(cell.line - 1, cell.column + 1);
            if( testCellUpRight.value == typeValue ||  testCellUpRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpRight);
            }
        }
        if(cell.line + 1 <= 9  && cell.column - 1 >= 0)
        {
            Cell& testCellDownLeft = this->grid.GetCellFromCoords(cell.line + 1, cell.column - 1);
            if( testCellDownLeft.value == typeValue ||  testCellDownLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownLeft);
            }
        }
        if(cell.line + 1 <= 9  && cell.column + 1 <= 9)
        {
            Cell& testCellDownRight = this->grid.GetCellFromCoords(cell.line + 1, cell.column + 1);
            if( testCellDownRight.value == typeValue ||  testCellDownRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownRight);
            }
        }
    return tabCells;
}

std::vector<Cell> CheckerGame::GetDameNeighbours(Cell &cell, std::string typeValue)
{
    std::vector<Cell> tabCells;
    std::string upperTypeValue = typeValue;
    for (auto &c: upperTypeValue) c = toupper(c);

    std::string upperCellValue = cell.value;
    for (auto &c: upperCellValue) c = toupper(c);

    int i = 1;
    bool test = true;
    bool canUpleft = true;
    bool canUpRight = true;
    bool canDownLeft = true;
    bool canDownRight = true;
    while(test)
    {
        if(cell.line - i >= 0 && cell.column - i >= 0 && canUpleft)
        {
            Cell testCellUpLeft = this->grid.GetCellFromCoords(cell.line - i, cell.column - i);
            if(testCellUpLeft.value == cell.value || testCellUpLeft.value == upperCellValue)
            {
                canUpleft = false;
            }
            if( testCellUpLeft.value == typeValue ||  testCellUpLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpLeft);
            }
        }
        else if(cell.line - i >= 0 && cell.column + i <= 9 && canUpRight)
        {
            Cell testCellUpRight = this->grid.GetCellFromCoords(cell.line - i, cell.column + i);
            if(testCellUpRight.value == cell.value || testCellUpRight.value == upperCellValue)
            {
                canUpRight = false;
            }
            if( testCellUpRight.value == typeValue ||  testCellUpRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpRight);
            }
        }
        else if(cell.line + i <= 9  && cell.column - i >= 0 && canDownLeft)
        {
            Cell testCellDownLeft = this->grid.GetCellFromCoords(cell.line + i, cell.column - i);
            if(testCellDownLeft.value == cell.value || testCellDownLeft.value == upperCellValue)
            {
                canDownLeft = false;
            }
            if( testCellDownLeft.value == typeValue ||  testCellDownLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownLeft);
            }
        }
        else if(cell.line + i <= 9  && cell.column + i <= 9 && canDownRight)
        {
            Cell testCellDownRight = this->grid.GetCellFromCoords(cell.line + i, cell.column + i);
            if(testCellDownRight.value == cell.value || testCellDownRight.value == upperCellValue)
            {
                canDownRight = false;
            }
            if( testCellDownRight.value == typeValue ||  testCellDownRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownRight);
            }
        }
        if(cell.line - i < 0 && cell.column - i < 0 && cell.line + i > 9  && cell.column + i > 9)
        {
            test = false;
        }
        i++;
    }
    return tabCells;
}

std::vector<Cell> CheckerGame::GetCellDestinations(Cell &cell, std::string typeValue)
{
    std::vector<Cell> tabCells;
    std::string upperTypeValue = typeValue;
    for (auto &c: upperTypeValue) c = toupper(c);

    if(cell.value == "b")
    {
        if(cell.line - 1 >= 0 && cell.column - 1 >= 0)
        {
            Cell testCellUpLeft = this->grid.GetCellFromCoords(cell.line - 1, cell.column - 1);
            if( testCellUpLeft.value == typeValue ||  testCellUpLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpLeft);
            }
        }
        if(cell.line - 1 >= 0 && cell.column + 1 <= 9)
        {
            Cell testCellUpRight = this->grid.GetCellFromCoords(cell.line - 1, cell.column + 1);
            if( testCellUpRight.value == typeValue ||  testCellUpRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellUpRight);
            }
        }
    }
    else if(cell.value == "n")
    {
        if(cell.line + 1 <= 9  && cell.column - 1 >= 0)
        {
            Cell testCellDownLeft = this->grid.GetCellFromCoords(cell.line + 1, cell.column - 1);
            if( testCellDownLeft.value == typeValue ||  testCellDownLeft.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownLeft);
            }
        }
        if(cell.line + 1 <= 9  && cell.column + 1 <= 9)
        {
            Cell testCellDownRight = this->grid.GetCellFromCoords(cell.line + 1, cell.column + 1);
            if( testCellDownRight.value == typeValue ||  testCellDownRight.value == upperTypeValue)
            {
                tabCells.push_back(testCellDownRight);
            }
        }
    }
    return tabCells;
}

std::vector<Cell> CheckerGame::GetAttackableNeighbours(Cell& cell)
{
    std::vector<Cell> tabCells;
    if(cell.value == "B")
    {
        std::vector<Cell> tabCells = GetDameNeighbours(cell, "n");
        int numberOfNeighbours = static_cast<int>(tabCells.size());

        if(numberOfNeighbours != 0)
        {
            for(int i = 0; i < numberOfNeighbours; i++)
            {
                int vecteurLine = tabCells[i].line + tabCells[i].line - cell.line;
                int vecteurColumn = tabCells[i].column + tabCells[i].column - cell.column;
                if(vecteurLine >= 0 && vecteurLine <= 9 && vecteurColumn >= 0 && vecteurColumn <= 9)
                {
                    Cell vecteurCell = this->grid.GetCellFromCoords(vecteurLine, vecteurColumn);
                    if(vecteurCell.value == "0")
                    {
                        tabCells.push_back(vecteurCell);
                    }
                }
            }
        }
    }
    else if(cell.value == "N")
    {
        std::vector<Cell> tabCells = GetDameNeighbours(cell, "b");
        int numberOfNeighbours = static_cast<int>(tabCells.size());
        if(numberOfNeighbours != 0)
        {
            for(int i = 0; i < numberOfNeighbours; i++)
            {
                int vecteurLine = tabCells[i].line + tabCells[i].line - cell.line;
                int vecteurColumn = tabCells[i].column + tabCells[i].column - cell.column;
                if(vecteurLine >= 0 && vecteurLine <= 9 && vecteurColumn >= 0 && vecteurColumn <= 9)
                {
                    Cell& vecteurCell = this->grid.GetCellFromCoords(vecteurLine, vecteurColumn);
                    if(vecteurCell.value == "0")
                    {
                        tabCells.push_back(vecteurCell);
                    }
                }
            }
        }
    }
    else if(cell.line == 'b')
    {
        std::vector<Cell> tabCells = GetCellNeighbours(cell, "n");
        int numberOfNeighbours = static_cast<int>(tabCells.size());
        if(numberOfNeighbours != 0)
        {
            for(int i = 0; i < numberOfNeighbours; i++)
            {
                int vecteurLine = tabCells[i].line + tabCells[i].line - cell.line;
                int vecteurColumn = tabCells[i].column + tabCells[i].column - cell.column;
                if(vecteurLine >= 0 && vecteurLine <= 9 && vecteurColumn >= 0 && vecteurColumn <= 9)
                {
                    Cell vecteurCell = this->grid.GetCellFromCoords(vecteurLine, vecteurColumn);
                    if(vecteurCell.value == "0")
                    {
                        tabCells.push_back(vecteurCell);
                    }
                }
            }
        }
    }
    else if(cell.line == 'n')
    {
        std::vector<Cell> tabCells = GetCellNeighbours(cell, "b");
        int numberOfNeighbours = static_cast<int>(tabCells.size());
        if(numberOfNeighbours != 0)
        {
            for(int i = 0; i < numberOfNeighbours; i++)
            {
                int vecteurLine = tabCells[i].line + tabCells[i].line - cell.line;
                int vecteurColumn = tabCells[i].column + tabCells[i].column - cell.column;
                if(vecteurLine >= 0 && vecteurLine <= 9 && vecteurColumn >= 0 && vecteurColumn <= 9)
                {
                    Cell vecteurCell = this->grid.GetCellFromCoords(vecteurLine, vecteurColumn);
                    if(vecteurCell.value == "0")
                    {
                        tabCells.push_back(vecteurCell);
                    }
                }
            }
        }
    }
    return tabCells;
}

void CheckerGame::PlayTurn()
{
    for (int i = 0; i < 2; i++)
    {
        CheckerPlayer currentPlayer = this->players[i];
        std::string upperPlayerValue = currentPlayer.value;
        for (auto &c: upperPlayerValue) c = toupper(c);

        std::cout << "C'est à " << currentPlayer.name << " de jouer : " << std::endl;

        int numberOfPawns = static_cast<int>(currentPlayer.cells.size());
        for (int j = 0; j < numberOfPawns; j++)
        {
            Cell currentPawn = currentPlayer.cells.at(j);
            std::vector<Cell> emptyNeighbours = GetCellNeighbours(currentPawn, "0");
            int numberOfEmptyNeighbours = static_cast<int>(emptyNeighbours.size());

            if (numberOfEmptyNeighbours != 0)
            {
                std::cout << "Le pion [" << currentPawn.line << "," << currentPawn.column << "] peut se déplacer vers : "  << std::endl;
                for (int k = 0; k < numberOfEmptyNeighbours; k++)
                {
                    Cell emptyNeighbour = emptyNeighbours.at(k);
                    std::cout << " - [" << emptyNeighbour.line << "," << emptyNeighbour.column << "]" << std::endl;
                }
            }
        }

        grid.Display();

        bool isInputValid = false;
        Cell& selectedPawn = this->HandlePlayerInput();
        while (!isInputValid)
        {
            if (selectedPawn.value == currentPlayer.value || selectedPawn.value == upperPlayerValue)
            {
                std::vector<Cell> emptyNeighbours = this->GetCellNeighbours(selectedPawn, "0");
                int numberOfEmptyNeighbours = static_cast<int>(emptyNeighbours.size());

                std::vector<Cell> attackableNeighbours = this->GetAttackableNeighbours(selectedPawn);
                int numberOfAttackableNeighbours = static_cast<int>(attackableNeighbours.size());

                if (numberOfAttackableNeighbours != 0 || numberOfEmptyNeighbours != 0)
                {
                    isInputValid = true;
                }
            }
            else
            {
                std::cout << "Saisie invalide : Le pion ne vous appartient pas ou ne pas ni attaquer ou se déplacer." << std::endl;
                selectedPawn = this->HandlePlayerInput();
            }
        }

        // Si le pion peut se déplacer
        std::vector<Cell> cellDestinations = GetCellDestinations(selectedPawn, "0");
        int numberOfDestinations = static_cast<int>(cellDestinations.size());

        if (numberOfDestinations != 0)
        {
            std::cout << "Destinations possibles :" << std::endl;
            for (int k = 0; k < numberOfDestinations; k++)
            {
                Cell destination = cellDestinations.at(k);
                std::cout << " - [" << destination.line << "," << destination.column << "]" << std::endl;
            }
            isInputValid = false;
            Cell& selectedDestination = this->HandlePlayerInput();
            while (!isInputValid)
            {
                for (int k = 0; k < numberOfDestinations; k++)
                {
                    Cell destination = cellDestinations.at(k);
                    if (selectedDestination.line == destination.line && selectedDestination.column == destination.column)
                        isInputValid = true;
                }
                if (!isInputValid)
                {
                    std::cout << "Saisie invalide : Le pion ne ne peut pas se déplacer dans cette case." << std::endl;
                    selectedPawn = this->HandlePlayerInput();
                }
            }

            grid.MoveCell(selectedPawn, selectedDestination);
            std::cout << "" << std::endl;
        }

    }
}

bool CheckerGame::IsOver()
{
    return false;
}


Cell &CheckerGame::HandlePlayerInput()
{
    int line = 0;
    int column = 0;

    std::cout << "Saisissez le numéro de ligne du pion à sélectionner : " << std::endl;
    std::cin >> line;
    while (line < 0 || line > this->grid.length-1)
    {
        std::cout << "Saisie incorrecte, il faut être dans les bords de la grille. Réessayez :" << std::endl;
        std::cin >> line;
    }

    std::cout << "Saisissez le numéro de colonne du pion à sélectionner : " << std::endl;
    std::cin >> column;
    while (column < 0 || column > this->grid.length-1)
    {
        std::cout << "Saisie incorrecte, il faut être dans les bords de la grille. Réessayez :" << std::endl;
        std::cin >> column;
    }

    return this->grid.GetCellFromCoords(line, column);
}

bool CheckerGame::IsWonByPlayer(const Player &player)
{
    return false;
}
