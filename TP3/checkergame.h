#ifndef CHECKERGAME_H
#define CHECKERGAME_H

#include "game.h"
#include "checkergrid.h"
#include "checkerplayer.h"

class CheckerGame : public Game
{
public:
    CheckerGame(CheckerGrid &grid);
    void InitGame();
    void PlayTurn();
    bool IsOver();

private:
    CheckerPlayer players[2];
    bool isOver = false;
    CheckerGrid& grid;
    Cell& HandlePlayerInput();
    bool IsWonByPlayer(const Player &player);
    std::vector<Cell> GetCellNeighbours(Cell &cell, std::string typeValue);
    std::vector<Cell> GetDameNeighbours(Cell &cell, std::string typeValue);
    std::vector<Cell> GetCellDestinations(Cell &cell, std::string typeValue);
    std::vector<Cell> GetAttackableNeighbours(Cell& cell);

};

#endif // CHECKERGAME_H
