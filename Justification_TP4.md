# TP4 : Justification
Justification du choix du projet pour faire le TP4.

## Problème de code de Claude Schlumberger

S : Le principe de responsabilité unique n’est pas respecté partout.
Les classes TicTacToeGrid et ConnectFourGrid ne le respectent pas car elles sont responsable d’une partie du fonctionnement de la classe Cell avec leurs méthodes IsCellEmpty et SetCellOwner. 
Il faudra rapatrier ces méthodes dans la classe Cell.

O : Le principe “Open/Closed” a été partiellement respecté dans l’implémentation du Puissance 4, en créant une nouvelle classe ConnectFourGrid au lieu de rajouter les fonctionnalités du jeu Puissance 4 dans la classe TicTacToeGrid. 
Pour le respecter entièrement, il aurait fallu créer une classe Grid, généralisation de TicTacToeGrid et ConnectFourGrid.

L : Le principe de substitution de Liskov n’est pas enfreint car il n’y a pas encore de classe abstraite. 

I : Le principe de ségrégation des interfaces n’est pas enfreint car il n’y a pas encore d’interfaces.

D : Le principe d’inversion des dépendances n’est jamais respecté : les classes suivantes ont des dépendances concrètes, au lieu de dépendances abstraites :

- Cells
- ConnectFourGrid
- TicTacToeGrid
- Game

Il faudra donc créer des classes abstraites pour chacune des dépendances.

Le “Tell, don’t ask” et la loi de Démeter sont respectés.

## Problème du code de Martin Hoffmann

S : n’est pas respecté, par exemple dans la classe Ligne, qui prend aussi en charge le fonctionnement des diagonales et des colonnes.

O : est partiellement respecté dans l’implémentation du Puissance 4 : création d’une nouvelle classe grillePuissance au lieu de rajouter les fonctionnalités du jeu Puissance 4 dans la classe grilleMorpion.

L : n’a pas eu l’occasion d’être violé car on a encore aucune classe abstraite.

I : n’a pas eu l’occasion d’être violé car on a encore aucune interface n'est utilisée.

D : jamais respecté.

Tell don’t ask et la loi de Démeter ne sont pas respecté à certain endroit.

# Décision

En ce basant sur nos observations par rapport aux respect des principes SOLID, et de nos appréciations personnelles, nous choisissons de continuer en utilisant le projet de Claude [disponible ici](https://gitlab.com/Ezyran/ei-cnam-cpp).
Il nous semble qu’il y aura moins de modifications à faire, et que la logique du code ne devra pas être trop chamboulée.