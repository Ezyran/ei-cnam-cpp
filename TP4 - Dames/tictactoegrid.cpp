#include "tictactoegrid.h"
#include "iostream"

TicTacToeGrid::TicTacToeGrid() : CombinatoryGrid(3, 3)
{
    this->GenerateDiagonals();
}

void TicTacToeGrid::GenerateDiagonals()
{
    Cells diagonal1 = Cells();
    diagonal1.AddCell(this->GetCellFromCoords(0,0));
    diagonal1.AddCell(this->GetCellFromCoords(1,1));
    diagonal1.AddCell(this->GetCellFromCoords(2,2));
    this->diagonals.push_back(diagonal1);

    Cells diagonal2 = Cells();
    diagonal2.AddCell(this->GetCellFromCoords(0,2));
    diagonal2.AddCell(this->GetCellFromCoords(1,1));
    diagonal2.AddCell(this->GetCellFromCoords(2,0));
    this->diagonals.push_back(diagonal2);
}

bool TicTacToeGrid::IsLineOwnedByPlayer(int lineNum, const Player& player)
{
    return this->lines[lineNum].IsOwnedBy(player);
}

bool TicTacToeGrid::IsColumnOwnedByPlayer(int columnNum, const Player &player)
{
    return this->columns[columnNum].IsOwnedBy(player);
}

bool TicTacToeGrid::IsDiagonalOwnedByPlayer(int diagonalNum, const Player &player)
{
    return this->diagonals[diagonalNum].IsOwnedBy(player);
}
