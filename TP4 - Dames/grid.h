#ifndef GRID_H
#define GRID_H

#include <vector>
#include "cell.h"
#include "cells.h"
#include "player.h"

class Grid
{
public:
    const int length;
    const int width;
    Grid(int length, int width);
    Cell& GetCellFromCoords(int line, int col);
    void Display();
    std::vector<Cell> cells;

protected:
    void GenerateCells();
    void DisplayLineSeparator();
    void DisplayGridHeader();
};

#endif // GRID_H
