#include <iostream>
#include "checkergame.h"

CheckerGame::CheckerGame(CheckerGrid &grid) : Game(grid), grid(grid)
{
    InitGame();
}

void CheckerGame::PlayTurn()
{
    if (players[0].cells.size() == 0)
    {
        std::cout << "Le joueur Un n'a plus de pions ! Il a perdu !" << std::endl;
        isOver = true;
        return;
    }
    if (players[1].cells.size() == 0)
    {
        std::cout << "Le joueur Deux n'a plus de pions ! Il a perdu !" << std::endl;
        isOver = true;
        return;
    }
    for (int i = 0; i < 2; i++)
    {
        std::cout << "C'est au tour du joueur " << i+1 << " " << players[i].name << " :" << std::endl;
        std::vector<Move> movements = std::vector<Move>();
        std::vector<Move> attacks = std::vector<Move>();

        grid.Display();

        int numberOfPawns = static_cast<int>(grid.cells.size());
        for (int j = 0; j < numberOfPawns; j++)
        {
            Cell& currentCell = grid.cells[j];
            if (currentCell.value == players[i].value)
            {
                std::vector<Move> cellAttacks = GetCellAttacks(currentCell);
                int numberOfCellAttacks = static_cast<int>(cellAttacks.size());
                for (int k = 0; k < numberOfCellAttacks; k++)
                {
                    Move &attack = cellAttacks[k];
                    attacks.push_back(attack);
                    std::cout << std::endl;
                }

                if (players[i].value == 1)
                {
                    std::vector<Move> cellMovements = GetCellMovementsPlayerOne(currentCell);
                    int numberOfCellMovements = static_cast<int>(cellMovements.size());
                    for (int k = 0; k < numberOfCellMovements; k++)
                    {
                        movements.push_back(cellMovements[k]);
                    }
                }
                else if (players[i].value == 2)
                {
                    std::vector<Move> cellMovements = GetCellMovementsPlayerTwo(currentCell);
                    int numberOfCellMovements = static_cast<int>(cellMovements.size());
                    for (int k = 0; k < numberOfCellMovements; k++)
                    {
                        movements.push_back(cellMovements[k]);
                    };
                }
            }
        }

        if (attacks.size() > 0)
        {
          int numberOfAttacks = static_cast<int>(attacks.size());
          for (int j = 0; j < numberOfAttacks; j++)
          {
              std::cout << " [" << j << "] " << attacks[i].ToString() << std::endl;
          }

          Move selection = MoveSelection(attacks);
          ExecuteMove(selection);
          attacks = GetCellAttacks(selection.destination);

        }
        else if (movements.size () > 0)
        {
            int numberOfMovements = static_cast<int>(movements.size());
            for (int j = 0; j < numberOfMovements; j++)
            {
                std::cout << " [" << j << "] " << movements[j].ToString() << std::endl;
            }

            Move selection = MoveSelection(movements);
            ExecuteMove(selection);
        }
        else
        {
            std::cout << "Le joueur " << i+1 << " ne peut plus jouer. Il a perdu !" << std::endl;
            this->isOver = true;
        }
    }
}

bool CheckerGame::IsOver()
{
    return isOver;
}

void CheckerGame::InitGame()
{
    CheckerPlayer player1;
    player1.value = 1;
    std::cout << "Saississez le nom du joueur 1 : " << std::endl;
    std::cin >> player1.name;

    CheckerPlayer player2;
    player2.value = 2;
    std::cout << "Saississez le nom du joueur 2 : " << std::endl;
    std::cin >> player2.name;

    this->players[0] = player1;
    this->players[1] = player2;

    // Placer des pions noirs
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (i != j && ((i%2 != 0 && j%2 == 0) || (i%2 == 0 && j%2 != 0)))
                {
                    Cell& blackCell = this->grid.GetCellFromCoords(i,j);
                    blackCell.value = 1;
                    this->players[0].cells.push_back(blackCell);
                }
            }
        }

        // Placer des pions blancs
        for (int i = 6; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (i != j && ((i%2 != 0 && j%2 == 0) || (i%2 == 0 && j%2 != 0)))
                {
                    Cell& whiteCell = this->grid.GetCellFromCoords(i,j);
                    whiteCell.value = 2;
                    this->players[1].cells.push_back(whiteCell);
                }
            }
        }

}

void CheckerGame::RemoveCellFromPlayer(Cell &cell)
{
    for (int i = 0; i < 2; i++)
    {
        int numberOfCells = static_cast<int>(this->players[i].cells.size());
        for (int j = 0; j < numberOfCells; j++)
        {
            Cell& currentCell = players[i].cells[j].get();
            if (currentCell.line == cell.line && currentCell.column == cell.column && currentCell.value == cell.value )
            {
                players[i].cells.erase(players[i].cells.begin()+j);
                return;
            }
        }
    }
}

std::vector<Move> CheckerGame::GetCellMovementsPlayerOne(Cell &cell)
{
    std::vector<Move> movements = std::vector<Move>();
    Cell& lowerLeftNeighbour = grid.GetCellLowerLeftNeighbour(cell);
    Cell& lowerRightNeighbour = grid.GetCellLowerRightNeighbour(cell);

    if (lowerLeftNeighbour.value == 0 && lowerLeftNeighbour.value != cell.value)
    {
        movements.push_back(Move(Move::MOVEMENT, cell, lowerLeftNeighbour));
    }
    if (lowerRightNeighbour.value == 0 && lowerRightNeighbour.value != cell.value)
    {
        movements.push_back(Move(Move::MOVEMENT, cell, lowerRightNeighbour));
    }
    return movements;
}

std::vector<Move> CheckerGame::GetCellMovementsPlayerTwo(Cell &cell)
{
    std::vector<Move> movements = std::vector<Move>();
    Cell& upperLeftNeighbour = grid.GetCellUpperLeftNeighbour(cell);
    Cell& upperRightNeighbour = grid.GetCellUpperRightNeighbour(cell);

    if (upperLeftNeighbour.value == 0 && upperLeftNeighbour.value != cell.value)
    {
        movements.push_back(Move(Move::MOVEMENT, cell, upperLeftNeighbour));
    }
    if (upperRightNeighbour.value == 0 && upperRightNeighbour.value != cell.value)
    {
       movements.push_back(Move(Move::MOVEMENT, cell, upperRightNeighbour));
    }
    return movements;
}

std::vector<Move> CheckerGame::GetCellAttacks(Cell &cell)
{
    std::vector<Move> attacks = std::vector<Move>();
    Cell& upperLeftNeighbour = grid.GetCellUpperLeftNeighbour(cell);
    Cell& upperRightNeighbour = grid.GetCellUpperRightNeighbour(cell);
    Cell& lowerLeftNeighbour = grid.GetCellLowerLeftNeighbour(cell);
    Cell& lowerRightNeighbour = grid.GetCellLowerRightNeighbour(cell);

    if (upperLeftNeighbour.value != 0 && upperLeftNeighbour.value != cell.value)
    {
        Cell& destination = grid.GetCellUpperLeftNeighbour(upperLeftNeighbour);
        if (destination.value == 0)
        {
            Move attack = Move(Move::ATTACK, cell, destination);
            attack.AddVictim(upperLeftNeighbour);
            attacks.push_back(attack);
        }
    }
    if (upperRightNeighbour.value != 0 && upperRightNeighbour.value != cell.value)
    {
        Cell& destination = grid.GetCellUpperRightNeighbour(upperRightNeighbour);
        if (destination.value == 0)
        {
            Move attack = Move(Move::ATTACK, cell, destination);
            attack.AddVictim(upperRightNeighbour);
            attacks.push_back(attack);
        }
    }
    if (lowerLeftNeighbour.value != 0 && lowerLeftNeighbour.value != cell.value)
    {
        Cell& destination = grid.GetCellLowerLeftNeighbour(lowerLeftNeighbour);
        if (destination.value == 0)
        {
            Move attack = Move(Move::ATTACK, cell, destination);
            attack.AddVictim(lowerLeftNeighbour);
            attacks.push_back(attack);
        }
    }
    if (lowerRightNeighbour.value != 0 && lowerRightNeighbour.value != cell.value)
    {
        Cell& destination = grid.GetCellLowerRightNeighbour(lowerRightNeighbour);
        if (destination.value == 0)
        {
            Move attack = Move(Move::ATTACK, cell, destination);
            attack.AddVictim(lowerRightNeighbour);
            attacks.push_back(attack);
        }
    }
    return attacks;
}

void CheckerGame::ExecuteMove(Move move)
{
    grid.MoveCell(move.source, move.destination);
    for (int i = 0; i < move.victims.GetCellsCount(); i++)
    {
        RemoveCellFromPlayer(move.GetVictims()[i]);
    }
}

Move CheckerGame::MoveSelection(std::vector<Move> moves)
{
    int numberOfMoves = static_cast<int>(moves.size());
    std::cout << "Veuillez sélectionner un coup à effectuer parmi les choix précédents :" << std::endl;
    int selection = -1;
    std::cin >> selection;
    while (selection < 0  || selection > numberOfMoves)
    {
        std::cout << "Saisie incorrecte. Réessayez :" << std::endl;
        std::cin >> selection;
    }
    return moves[selection];
}

