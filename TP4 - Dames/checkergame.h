#ifndef CHECKERGAME_H
#define CHECKERGAME_H

#include "game.h"
#include "checkergrid.h"
#include "move.h"
#include "checkerplayer.h"

class CheckerGame : public Game
{
public:
    CheckerGame(CheckerGrid& grid);
    void PlayTurn() override;
    bool IsOver() override;
    void InitGame() override;

protected:
    CheckerGrid &grid;
    CheckerPlayer players[2];
    void RemoveCellFromPlayer(Cell& cell);
    std::vector<Move> GetCellMovementsPlayerOne(Cell &cell);
    std::vector<Move> GetCellMovementsPlayerTwo(Cell &cell);
    std::vector<Move> GetCellAttacks(Cell &cell);
    void ExecuteMove(Move move);
    Move MoveSelection(std::vector<Move> moves);
};

#endif // CHECKERGAME_H
