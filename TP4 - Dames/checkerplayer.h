#ifndef CHECKERPLAYER_H
#define CHECKERPLAYER_H

#include <vector>
#include "cell.h"
#include "player.h"

struct CheckerPlayer : public Player
{
    std::vector<std::reference_wrapper<Cell>> cells = std::vector<std::reference_wrapper<Cell>>();
};

#endif // CHECKERPLAYER_H
