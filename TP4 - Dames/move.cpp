#include "move.h"
#include <sstream>
#include <string>

Move::Move(Move::Type type, Cell &source, Cell &destination, Cells victims) : type(type), source(source), destination(destination), victims(victims) {}

Move::Move(Move::Type type, Cell &source, Cell &destination) : type(type), source(source), destination(destination) {}

std::string Move::ToString()
{
    std::string res;
    std::string action;
    if (this->type == this->MOVEMENT)
    {
        action = "se déplacer";
    }
    else
    {
        action = "attaquer";
    }
    res = "Le pion en [" + std::to_string(source.line) + "," + std::to_string(source.column) + "] peut " + action + " vers [" + std::to_string(destination.line) + "," + std::to_string(destination.column) + "].";
    return res;
}
