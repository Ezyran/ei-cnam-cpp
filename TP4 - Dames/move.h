#ifndef MOVE_H
#define MOVE_H

#include "cell.h"
#include "cells.h"

class Move
{
public:
    enum Type {
        MOVEMENT,
        ATTACK
    };

    Move(Move::Type type, Cell &source, Cell &destination, Cells victims);
    Move(Move::Type type, Cell &source, Cell &destination);

    Move::Type type;
    Cell &source;
    Cell &destination;
    Cells victims;

    std::string ToString();
    void AddVictim(Cell victim) { this->victims.AddCell(victim); }
    std::vector<std::reference_wrapper<Cell>> GetVictims() { return victims.GetCells(); }

};

#endif // MOVE_H
