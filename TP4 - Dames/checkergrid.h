#ifndef CHECKERGRID_H
#define CHECKERGRID_H

#include "grid.h"
#include "cell.h"

class CheckerGrid : public Grid
{
public:
    CheckerGrid();

    Cell& GetCellUpperRightNeighbour(Cell& cell);
    Cell& GetCellUpperLeftNeighbour(Cell& cell);
    Cell& GetCellLowerRightNeighbour(Cell& cell);
    Cell& GetCellLowerLeftNeighbour(Cell& cell);

    void MoveCell(Cell& source, Cell& destination);
    void RemoveCell(Cell& cell);
    int GetPositionCell(Cell& cell);
};

#endif // CHECKERGRID_H
