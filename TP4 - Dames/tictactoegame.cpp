#include <iostream>
#include <string>
#include "tictactoegame.h"



TicTacToeGame::TicTacToeGame(TicTacToeGrid &grid) : CombinatoryGame(grid), grid(grid) {}

void TicTacToeGame::PlayTurn()
{
    for (int i = 0; i < 2; i++)
    {
        this->grid.Display();
        Player currentPlayer = this->players[i];
        std::cout << "C'est à " << currentPlayer.name << " de jouer : " << std::endl;

        Cell& cell = this->HandlePlayerInput();
        bool cellIsValid = cell.IsEmpty();
        while (!cellIsValid)
        {
            std::cout << "Il faut désigner une case vide !" << std::endl;
            cell = this->HandlePlayerInput();
            cellIsValid = cell.IsEmpty();
        }

        cell.value = currentPlayer.value;

        if (this->IsWonByPlayer(currentPlayer))
            std::cout << "Félicitations ! " << currentPlayer.name << " a gagné la partie !" << std::endl;

        if (this->grid.IsFull())
            std::cout << "Fin de la partie ! La grille est pleine ..." << std::endl;

        this->isOver = this->IsWonByPlayer(currentPlayer) || this->grid.IsFull();

        if (this->isOver)
            return;
    }
}

bool TicTacToeGame::IsOver()
{
    return this->isOver;
}

Cell &TicTacToeGame::HandlePlayerInput()
{
    int line = -1;
    std::cout << "Saisissez le numéro de ligne de la case à sélectionner : " << std::endl;
    std::cin >> line;
    while (line < 0  || line > this->grid.length)
    {
        std::cout << "Saisie incorrecte, il faut être dans les bords de la grille. Réessayez :" << std::endl;
        std::cin >> line;
    }

    int column = -1;
    std::cout << "Saisissez le numéro de colonne de la case à sélectionner : " << std::endl;
    std::cin >> column;
    while (column < 0 || column > this->grid.length)
    {
        std::cout << "Saisie incorrecte, il faut être dans les bords de la grille. Réessayez :" << std::endl;
        std::cin >> column;
    }

    return this->grid.GetCellFromCoords(line, column);
}
