#ifndef CONNECTFOURGRID_H
#define CONNECTFOURGRID_H

#include <vector>
#include "player.h"
#include "combinatorygrid.h"

class ConnectFourGrid: public CombinatoryGrid
{
public:
    ConnectFourGrid();
    bool IsLineOwnedByPlayer(int lineNum, const Player& player);
    bool IsColumnOwnedByPlayer(int columnNum, const Player &player);
    bool IsDiagonalOwnedByPlayer(int diagonalNum, const Player &player);

private:
    void GenerateDiagonals();

};

#endif // CONNECTFOURGRID_H
