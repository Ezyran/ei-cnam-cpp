#include "game.h"
#include <iostream>
#include <string>

Game::Game(Grid &grid) : grid(grid) {}

void Game::InitGame()
{
    Player player1;
    player1.value = 1;
    std::cout << "Saississez le nom du joueur 1 : " << std::endl;
    std::cin >> player1.name;

    Player player2;
    player2.value = 2;
    std::cout << "Saississez le nom du joueur 2 : " << std::endl;
    std::cin >> player2.name;

    this->players[0] = player1;
    this->players[1] = player2;
}
