#include "cells.h"

bool Cells::IsOwnedBy(const Player& player)
{
    for (int i = 0; i < GetCellsCount(); i++)
    {
        Cell& cell = cells[i].get();
        if (cell.value != player.value)
            return false;
    }
    return true;
}

int Cells::NumberOfCellsOwnedByPlayer(const Player &player)
{
    int count = 0;
    for (int i = 0; i < GetCellsCount(); i++)
    {
        Cell& cell = cells[i].get();
        if (cell.value == player.value)
            count++;
    }
    return count;
}
