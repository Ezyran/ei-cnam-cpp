#include "grid.h"
#include "iostream"

Grid::Grid(int length, int width) : length(length), width(width)
{
    this->GenerateCells();
}

void Grid::Display()
{
    DisplayGridHeader();
    for (int i = 0; i < this->length; i++)
    {
        DisplayLineSeparator();
        std::cout << " " << i;
        for (int j = 0; j < this->width; j++)
        {
            std::string cellValue = std::to_string(this->GetCellFromCoords(i, j).value);
            std::cout << " | " << (cellValue == "0" ? " " : cellValue) ;

        }
        std::cout << " | " << std::endl;
    }
    DisplayLineSeparator();
}

void Grid::DisplayLineSeparator(){
    std::cout << "   ";
    for (int i = 0; i < this->width; i++)
    {
        std::cout << "+---";
    }
    std::cout << "+" << std::endl;
}

void Grid::DisplayGridHeader()
{
    std::cout << "  ";
    for (int i = 0; i < this->width; i++)
    {
        std::cout << "   " << i;
    }
    std::cout << std::endl;
}

void Grid::GenerateCells()
{
    for (int i = 0; i < this->length; i++)
    {
        for (int j = 0; j < this->width; j++)
        {
            Cell cell = Cell(i, j);
            this->cells.push_back(cell);
        }
    }
}

Cell& Grid::GetCellFromCoords(int line, int col)
{
    return this->cells.at(col+line*this->width);
}
