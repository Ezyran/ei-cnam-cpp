#include <iostream>
#include "checkergrid.h"

CheckerGrid::CheckerGrid() : Grid(10, 10)
{

}

Cell& CheckerGrid::GetCellUpperRightNeighbour(Cell& cell)
{
    if(cell.line - 1 < 0 || cell.column + 1 > 9)
    {
        return cell;
    }
    return GetCellFromCoords(cell.line - 1, cell.column + 1);
}

Cell& CheckerGrid::GetCellUpperLeftNeighbour(Cell& cell)
{
    if(cell.line - 1 < 0 || cell.column - 1 < 0)
    {
        return cell;
    }
    return GetCellFromCoords(cell.line - 1, cell.column - 1);
}

Cell& CheckerGrid::GetCellLowerRightNeighbour(Cell& cell)
{
    if(cell.line + 1 > 9 || cell.column + 1 > 9)
    {
        return cell;
    }
    return GetCellFromCoords(cell.line + 1, cell.column + 1);
}

Cell& CheckerGrid::GetCellLowerLeftNeighbour(Cell& cell)
{
    if(cell.line + 1 > 9 || cell.column - 1 < 0)
    {
        return cell;
    }
    return GetCellFromCoords(cell.line + 1, cell.column - 1);
}

void CheckerGrid::MoveCell(Cell& source, Cell& destination)
{
    Cell cellRessource = Cell(source.line, source.column);
    source.line = destination.line;
    source.column = destination.column;
    destination.line = cellRessource.line;
    destination.column = cellRessource.column;
    std::swap(cells[GetPositionCell(source)], cells[GetPositionCell(destination)]);
    std::cout << std::endl;
}

void CheckerGrid::RemoveCell(Cell& cell)
{
    cell.value = 0;
}

int CheckerGrid::GetPositionCell(Cell& cell)
{
    int position = cell.line * 10 + cell.column;
    return position;
}
