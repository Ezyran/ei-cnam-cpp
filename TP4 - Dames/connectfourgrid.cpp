#include "connectfourgrid.h"
#include "iostream"

ConnectFourGrid::ConnectFourGrid() : CombinatoryGrid(4, 7)
{
    this->GenerateDiagonals();
}

void ConnectFourGrid::GenerateDiagonals()
{
    Cells diagonal1 = Cells();
    diagonal1.AddCell(this->GetCellFromCoords(0,0));
    diagonal1.AddCell(this->GetCellFromCoords(1,1));
    diagonal1.AddCell(this->GetCellFromCoords(2,2));
    diagonal1.AddCell(this->GetCellFromCoords(3,3));
    this->diagonals.push_back(diagonal1);

    Cells diagonal2 = Cells();
    diagonal2.AddCell(this->GetCellFromCoords(0,1));
    diagonal2.AddCell(this->GetCellFromCoords(1,2));
    diagonal2.AddCell(this->GetCellFromCoords(2,3));
    diagonal2.AddCell(this->GetCellFromCoords(3,4));
    this->diagonals.push_back(diagonal2);

    Cells diagonal3 = Cells();
    diagonal3.AddCell(this->GetCellFromCoords(0,2));
    diagonal3.AddCell(this->GetCellFromCoords(1,3));
    diagonal3.AddCell(this->GetCellFromCoords(2,4));
    diagonal3.AddCell(this->GetCellFromCoords(3,5));
    this->diagonals.push_back(diagonal3);

    Cells diagonal4 = Cells();
    diagonal4.AddCell(this->GetCellFromCoords(0,3));
    diagonal4.AddCell(this->GetCellFromCoords(1,4));
    diagonal4.AddCell(this->GetCellFromCoords(2,5));
    diagonal4.AddCell(this->GetCellFromCoords(3,6));
    this->diagonals.push_back(diagonal4);

    Cells diagonal5 = Cells();
    diagonal5.AddCell(this->GetCellFromCoords(0,6));
    diagonal5.AddCell(this->GetCellFromCoords(1,5));
    diagonal5.AddCell(this->GetCellFromCoords(2,4));
    diagonal5.AddCell(this->GetCellFromCoords(3,3));
    this->diagonals.push_back(diagonal5);

    Cells diagonal6 = Cells();
    diagonal6.AddCell(this->GetCellFromCoords(0,5));
    diagonal6.AddCell(this->GetCellFromCoords(1,4));
    diagonal6.AddCell(this->GetCellFromCoords(2,3));
    diagonal6.AddCell(this->GetCellFromCoords(3,2));
    this->diagonals.push_back(diagonal6);

    Cells diagonal7 = Cells();
    diagonal7.AddCell(this->GetCellFromCoords(0,4));
    diagonal7.AddCell(this->GetCellFromCoords(1,3));
    diagonal7.AddCell(this->GetCellFromCoords(2,2));
    diagonal7.AddCell(this->GetCellFromCoords(3,1));
    this->diagonals.push_back(diagonal7);

    Cells diagonal8 = Cells();
    diagonal8.AddCell(this->GetCellFromCoords(0,3));
    diagonal8.AddCell(this->GetCellFromCoords(1,2));
    diagonal8.AddCell(this->GetCellFromCoords(2,1));
    diagonal8.AddCell(this->GetCellFromCoords(3,0));
    this->diagonals.push_back(diagonal8);
}

bool ConnectFourGrid::IsLineOwnedByPlayer(int lineNum, const Player& player)
{
    return lines[lineNum].NumberOfCellsOwnedByPlayer(player) >= 4;
}

bool ConnectFourGrid::IsColumnOwnedByPlayer(int columnNum, const Player &player)
{
    return columns[columnNum].NumberOfCellsOwnedByPlayer(player) >= 4;
}

bool ConnectFourGrid::IsDiagonalOwnedByPlayer(int diagonalNum, const Player &player)
{
    return diagonals[diagonalNum].NumberOfCellsOwnedByPlayer(player) >= 4;
}
