#include <iostream>
#include <vector>
#include <memory>
#include "checkergrid.h"
#include "checkergame.h"

int main()
{
    CheckerGrid grid = CheckerGrid();
    std::unique_ptr<Game> game(new CheckerGame(grid));

    while (!game->IsOver())
    {
        game->PlayTurn();
    }

}
