#ifndef CELLS_H
#define CELLS_H

#include <vector>
#include <functional>
#include "cell.h"
#include "player.h"

class Cells
{
public:
    bool IsOwnedBy(const Player& player);
    void AddCell(Cell& cell) { this->cells.push_back(cell); }
    int GetCellsCount() { return static_cast<int>(this->cells.size()); }
    int NumberOfCellsOwnedByPlayer(const Player& player);
    std::vector<std::reference_wrapper<Cell>> GetCells() { return this->cells; }
private:
    // Ensemble de références de cases
    std::vector<std::reference_wrapper<Cell>> cells = std::vector<std::reference_wrapper<Cell>>();
};

#endif // CELLS_H
