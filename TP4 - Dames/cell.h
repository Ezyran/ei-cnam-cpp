#ifndef CELL_H
#define CELL_H

#include "player.h"

class Cell
{
public:
    // Valeur de la case, indique le joueur propriétaire de la case.
    // Vaut 0 lorsque vide.
    Cell(int line, int column);
    int value = 0;
    int line;
    int column;
    bool IsOwnedByPlayer(Player &player) { return this->value == player.value; }
    bool IsEmpty() { return this->value == 0; }
    bool isNull() { return (this->column == -1 && this->line == -1); }
};

#endif // CELL_H
