#ifndef CONNECTFOURGAME_H
#define CONNECTFOURGAME_H
#include "connectfourgrid.h"
#include "combinatorygame.h"

class ConnectFourGame : public CombinatoryGame
{
public:
        ConnectFourGame(ConnectFourGrid &grid);
        void PlayTurn();
        bool IsOver();

protected:
        ConnectFourGrid &grid;
        Cell& HandlePlayerInput();
};

#endif // CONNECTFOURGAME_H
