#ifndef PLAYER_H
#define PLAYER_H

#include <string>

struct Player
{
    std::string name;
    int value;
};

#endif // PLAYER_H
