#include "combinatorygrid.h"

CombinatoryGrid::CombinatoryGrid(int length, int width) : Grid(length, width)
{
    this->GenerateLines();
    this->GenerateColumns();
}

std::vector<Cells> CombinatoryGrid::GetLines()
{
    return lines;
}

std::vector<Cells> CombinatoryGrid::GetColumns()
{
    return columns;
}

std::vector<Cells> CombinatoryGrid::GetDiagonals()
{
    return diagonals;
}

bool CombinatoryGrid::IsFull()
{
    int numberOfCells = static_cast<int>(cells.size());
    for (int i = 0; i < numberOfCells; i++)
    {
        if (cells[i].value == 0)
            return false;
    }
    return true;
}

void CombinatoryGrid::GenerateLines()
{
    for (int i = 0; i < length; i++)
    {
        Cells line = Cells();
        for (int j = 0; j < width; j++)
        {
            line.AddCell(GetCellFromCoords(i, j));
        }
        lines.push_back(line);
    }
}

void CombinatoryGrid::GenerateColumns()
{
    for (int i = 0; i < width; i++)
    {
        Cells column = Cells();
        for (int j = 0; j < length; j++)
        {
            column.AddCell(GetCellFromCoords(j, i));
        }
        columns.push_back(column);
    }
}
