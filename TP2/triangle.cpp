#include "triangle.h"
#include "floatextension.h"

Triangle::Triangle(Point cornerA, Point cornerB, Point cornerC)
{
    this->cornerA = cornerA;
    this->cornerB = cornerB;
    this->cornerC = cornerC;
}

Point Triangle::GetCornerA()
{
    return this->cornerA;
}

void Triangle::SetCornerA(const Point &newCornerA)
{
    this->cornerA = newCornerA;
}

Point Triangle::GetCornerB()
{
    return this->cornerB;
}

void Triangle::SetCornerB(const Point &newCornerB)
{
    this->cornerA = newCornerB;
}

Point Triangle::GetCornerC()
{
    return this->cornerC;
}

void Triangle::SetCornerC(const Point &newCornerC)
{
    this->cornerA = newCornerC;
}

float Triangle::GetBase()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    float base = lengths[0];
    if (Point::DistanceBetween(this->cornerA, this->cornerC) > base)
            base = lengths[1];
    if (Point::DistanceBetween(this->cornerB, this->cornerC) > base)
            base = lengths[2];
    return base;
}

float Triangle::GetPerimeter()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    return lengths[0] + lengths[1] + lengths[2];
}

float Triangle::GetArea()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    float halfPerimeter = this->GetPerimeter() / 2;

    return sqrt(halfPerimeter*(halfPerimeter-lengths[0])*(halfPerimeter-lengths[1])*(halfPerimeter-lengths[2]));
}

float Triangle::GetHeight()
{
    return 2 * this->GetArea() / this->GetBase();
}

bool Triangle::IsEquilateral()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    return FloatExtension::Equal(lengths[0], lengths[1]) && FloatExtension::Equal(lengths[0], lengths[2]);
}

bool Triangle::IsIsosceles()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    return FloatExtension::Equal(lengths[0], lengths[1]) ||
           FloatExtension::Equal(lengths[0], lengths[2]) ||
           FloatExtension::Equal(lengths[1], lengths[2]);
}

bool Triangle::IsRightAngled()
{
    std::array<float,3> lengths = this->GetSizesLengths();

    return FloatExtension::Equal(lengths[0]*lengths[0] + lengths[1]*lengths[1], this->GetBase()*this->GetBase()) ||
           FloatExtension::Equal(lengths[0]*lengths[0] + lengths[2]*lengths[2], this->GetBase()*this->GetBase()) ||
           FloatExtension::Equal(lengths[1]*lengths[1] + lengths[2]*lengths[2], this->GetBase()*this->GetBase());
}

void Triangle::Display()
{
    std::cout << "Coin A : " << std::endl;
    std::cout << " - PosX =\t" << this->cornerA.x << std::endl;
    std::cout << " - PosY =\t" << this->cornerA.y << std::endl;
    std::cout << "Coin B : " << std::endl;
    std::cout << " - PosX =\t" << this->cornerB.x << std::endl;
    std::cout << " - PosY =\t" << this->cornerB.y << std::endl;
    std::cout << "Coin C: " << std::endl;
    std::cout << " - PosX =\t" << this->cornerC.x << std::endl;
    std::cout << " - PosY =\t" << this->cornerC.y << std::endl;
    std::cout << "Base =\t\t" << this->GetBase() << std::endl;
    std::cout << "Perimeter =\t" << this->GetPerimeter() << std::endl;
    std::cout << "Surface =\t" << this->GetArea() << std::endl;
    std::cout << "Hauteur =\t" << this->GetHeight() << std::endl;
    std::cout << "Est Isocèle ? :\t" << this->IsIsosceles() << std::endl;
    std::cout << "Est Equilatéral ? :\t" << this->IsEquilateral() << std::endl;
    std::cout << "Est Rectangle ? :\t" << this->IsRightAngled() << std::endl;
    std::cout << std::endl;
}

std::array<float,3> Triangle::GetSizesLengths()
{
    std::array<float,3> res;
    res[0] = Point::DistanceBetween(this->GetCornerA(), this->GetCornerB());
    res[1] = Point::DistanceBetween(this->GetCornerA(), this->GetCornerC());
    res[2] = Point::DistanceBetween(this->GetCornerB(), this->GetCornerC());
    return res;
}


