#include <iostream>
#include "point.h"
#include "rectangle.h"
#include "triangle.h"
#include "circle.h"

int main()
{
    // Création de points de test
    Point pt_origine;
    pt_origine.x = 0.0f;
    pt_origine.y = 0.0f;

    Point pt_a;
    pt_a.x = 0.0f;
    pt_a.y = 2.0f;

    Point pt_b;
    pt_b.x = 1.0f;
    pt_b.y = 0.0f;

    Point pt_c;
    pt_c.x = -1.0f;
    pt_c.y = 0.0f;

    Point pt_d;
    pt_d.x = 2.0f;
    pt_d.y = 0.0f;

    // Initialisation et affichage d'un petit carré
    Rectangle rect_a = Rectangle(pt_a, 100, 100);
    std::cout << "#### Affichage du carré en A" << std::endl;
    rect_a.Display();

    // Initialisation et affichage d'un rectangle un peu plus grand
    Rectangle rect_b = Rectangle(pt_b, 300, 100);
    std::cout << "#### Affichage du rectangle en B" << std::endl;
    rect_b.Display();

    // Initialisation et affichage d'un triangle isocèle
    Triangle tri_iso = Triangle(pt_a, pt_b, pt_c);
    std::cout << "#### Affichage du triangle ABC" << std::endl;
    tri_iso.Display();

    // Initialisation et affichage d'un triangle rectangle
    Triangle tri_rect = Triangle(pt_a, pt_origine, pt_d);
    std::cout << "#### Affichage du triangle AOD" << std::endl;
    tri_rect.Display();

    // Initialisation et affichage d'un cercle
    Circle cercle = Circle(pt_origine, 2);
    std::cout << "#### Affichage du cercle de centre O" << std::endl;
    cercle.Display();

    // Test des différentes fonctions de la classe Circle
    std::string aSurCercle = cercle.IsOnCircle(pt_a) ? "Le point A est sur le cercle !" : "Le point A n'est pas sur le cercle";
    std::cout << aSurCercle << std::endl;

    std::string bDansCercle = cercle.IsInCircle(pt_b) ? "Le point B est dans le cercle !" : "Le point B n'est pas dans le cercle.";
    std::cout << bDansCercle << std::endl;

    std::string aDansCercle = cercle.IsInCircle(pt_a) ? "Le point A est dans le cercle !" : "Le point A n'est pas dans le cercle.";
    std::cout << aDansCercle << std::endl;

    return 0;
}
