#ifndef POINT_H
#define POINT_H

struct Point
{
    // Position X du point
    float x;
    // Position Y du point
    float y;
    // Fonction statique. Renvoie la distance entre les deux points passés en paramètres.
    static float DistanceBetween(Point a, Point b);
};

#endif // POINT_H
