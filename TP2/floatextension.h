#ifndef FLOATEXTENSION_H
#define FLOATEXTENSION_H

#include <cmath>

// Petite structure qui regroupe des fonctions de comparaison de floats
struct FloatExtension
{
    // Détermine si le premier float passé en paramètre est supérieur au deuxième.
    static bool IsGreaterThan(float a, float b);
    // Détermine si le premier float passé en paramètre est inférieur au deuxième.
    static bool IsLesserThan(float a, float b);
    // Détermine si le premier float passé en paramètre est égal au deuxième.
    static bool Equal(float a, float b);
};

#endif // FLOATEXTENSION_H
