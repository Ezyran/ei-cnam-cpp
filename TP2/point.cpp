#include "point.h"
#include <cmath>

float Point::DistanceBetween(Point a, Point b)
{
    float hDistance = a.x - b.x;
    float vDistance = a.y - b.y;
    return sqrtf(hDistance*hDistance + vDistance*vDistance);
}
