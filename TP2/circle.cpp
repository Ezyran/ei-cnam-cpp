#include "circle.h"
#include "floatextension.h"
#include <cmath>
#include <iostream>

Circle::Circle(const Point &center, int diameter)
{
    this->center = center;
    this->diameter = diameter;
}

Point Circle::GetCenter()
{
    return this->center;
}

void Circle::SetCenter(const Point &newCenter)
{
    this->center = newCenter;
}

int Circle::GetDiameter()
{
    return this->diameter;
}

void Circle::SetDiameter(int newDiameter)
{
    this->diameter = newDiameter;
}

float Circle::GetPerimeter()
{
    return M_PI * this->diameter;
}

float Circle::GetSurface()
{
    return M_PI * powf(this->diameter/2.0f, 2.0f);
}

bool Circle::IsOnCircle(const Point &point)
{
    return FloatExtension::Equal(Point::DistanceBetween(this->center, point), this->diameter);
}

bool Circle::IsInCircle(const Point &point)
{
    return FloatExtension::IsLesserThan(Point::DistanceBetween(this->center, point), this->diameter);
}

void Circle::Display()
{
    std::cout << "Centre : " << std::endl;
    std::cout << " - PosX =\t" << this->center.x << std::endl;
    std::cout << " - PosY =\t" << this->center.y << std::endl;
    std::cout << "Diamètre =\t" << this->diameter << std::endl;
    std::cout << "Perimètre =\t" << this->GetDiameter() << std::endl;
    std::cout << "Surface =\t" << this->GetSurface() << std::endl;
    std::cout << std::endl;
}

