#include "floatextension.h"

bool FloatExtension::IsGreaterThan(float a, float b)
{
    return (a - b) > 0;
}

bool FloatExtension::IsLesserThan(float a, float b)
{
    return (a - b) < 0;
}

bool FloatExtension::Equal(float a, float b)
{
    return std::abs(a - b) < 0.001f;
}
