#ifndef CIRCLE_H
#define CIRCLE_H

#include "point.h"

class Circle
{
public:
    // Constructeur de cercle.
    Circle(const Point &center, int diameter);

    // Getter public du centre du cercle
    Point GetCenter();
    // Setter public du centre du cercle
    void SetCenter(const Point &newCenter);

    // Getter public du centre du cercle
    int GetDiameter();
    // Setter public du centre du cercle
    void SetDiameter(int newDiameter);

    // Renvoie le périmètre du cercle
    float GetPerimeter();

    // Renvoie la surface du cercle
    float GetSurface();

    // Détermine si le point passé en paramètre se trouve sur le cercle.
    bool IsOnCircle(const Point &point);

    // Détermine si le point passé en paramètre se trouve dans le cercle.
    bool IsInCircle(const Point &point);

    // Affiche le cercle.
    void Display();

private:
    // Centre du cercle
    Point center;
    // Diamètre du cercle
    int diameter;
};

#endif // CIRCLE_H
