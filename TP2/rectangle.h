#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point.h"

class Rectangle
{
public:
    // Constructeur de cercle
    Rectangle(const Point &upperLeftCorner, int length, int width);

    // Getter de la longueur du rectangle
    int GetLength();
    // Setter de la longueur du rectangle
    void SetLength(int newLength);

    // Getter de la largeur du rectangle
    int GetWidth();
    // Setter de la largeur du rectangle
    void SetWidth(int newWidth);

    // Getter du coin supérieur gauche
    Point GetUpperLeftCorner();
    // Setter du coin supérieur gauche
    void SetUpperLeftCorner(const Point &newCorner);

    // Renvoie le périmètre du rectangle
    int GetPerimeter();

    // Renvoie la surface du rectangle
    int GetArea();

    // Détermine si le recntangle passé en paramètre a un plus grand périmètre
    bool HasBiggerPerimeterThan(Rectangle rect);

    // Détermine si le rectangle passé en paramètre a une surface plus grande
    bool IsLargerThan(Rectangle rect);

    // Affichage du rectangle
    void Display();

private:
    // Longueur du rectangle
    int length;
    // Largeur du rectangle
    int width;
    // Coin supérieur gauche du rectangle
    Point upperLeftCorner;
};

#endif // RECTANGLE_H
