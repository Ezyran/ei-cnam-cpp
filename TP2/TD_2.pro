TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        circle.cpp \
        floatextension.cpp \
        main.cpp \
        point.cpp \
        rectangle.cpp \
        triangle.cpp

HEADERS += \
    circle.h \
    floatextension.h \
    point.h \
    rectangle.h \
    triangle.h
