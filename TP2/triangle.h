#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "point.h"
#include <cmath>
#include <array>
#include <iostream>

class Triangle
{
public:
    // Constructeur de triangle
    Triangle(Point cornerA, Point cornerB, Point cornerC);

    // Getter du point A
    Point GetCornerA();
    // Setter du point A
    void SetCornerA(const Point& newCornerA);

    // Getter du point B
    Point GetCornerB();
    // Setter du point B
    void SetCornerB(const Point& newCornerB);

    // Getter du point C
    Point GetCornerC();
    // Setter du point C
    void SetCornerC(const Point& newCornerC);

    // Renvoie le périmètre du triangle
    float GetPerimeter();

    // Renvoie la longueur de la base du triangle
    float GetBase();

    // Renvoie la hauteur du triangle
    float GetHeight();

    // Renvoie la surface du triangle
    float GetArea();

    // Détermine si le triangle est isocèle
    bool IsIsosceles();

    // Détermine si le triangle est rectangle
    bool IsRightAngled();

    // Détermine si le triangle est équilatéral
    bool IsEquilateral();

    // Affichage du triangle
    void Display();

private:
    // Point A du triangle
    Point cornerA;
    // Point B du triangle
    Point cornerB;
    // Point C du triangle
    Point cornerC;

    // Fonction privée qui renvoie le tableau de longueurs des côtés du triangle
    std::array<float,3> GetSizesLengths();
};

#endif // TRIANGLE_H
