#include <iostream>
#include "rectangle.h"

Rectangle::Rectangle(const Point &upperLeftCorner, int length, int width)
{
    this->upperLeftCorner = upperLeftCorner;
    this->length = length;
    this->width = width;
}

int Rectangle::GetLength()
{
    return this->length;
}

void Rectangle::SetLength(int newLength)
{
    this->length = newLength;
}

int Rectangle::GetWidth()
{
    return this->width;
}

void Rectangle::SetWidth(int newWidth)
{
    this->width = newWidth;
}

Point Rectangle::GetUpperLeftCorner()
{
    return this->upperLeftCorner;
}

void Rectangle::SetUpperLeftCorner(const Point &newCorner)
{
    this->upperLeftCorner = newCorner;
}

int Rectangle::GetPerimeter()
{
    return (this->width + this->length)*2;
}

int Rectangle::GetArea()
{
    return this->width * this->length;
}

bool Rectangle::HasBiggerPerimeterThan(Rectangle rect)
{
    return this->GetPerimeter() > rect.GetPerimeter();
}

bool Rectangle::IsLargerThan(Rectangle rect)
{
    return this->GetArea() > rect.GetArea();
}

void Rectangle::Display()
{
    std::cout << "Upper Left Corner : " << std::endl;
    std::cout << " - PosX =\t" << this->upperLeftCorner.x << std::endl;
    std::cout << " - PosY =\t" << this->upperLeftCorner.y << std::endl;
    std::cout << "Length =\t" << this->length << std::endl;
    std::cout << "Width =\t\t" << this->width << std::endl;
    std::cout << "Perimeter =\t" << this->GetPerimeter() << std::endl;
    std::cout << "Area =\t\t" << this->GetArea() << std::endl;
    std::cout << std::endl;
}
