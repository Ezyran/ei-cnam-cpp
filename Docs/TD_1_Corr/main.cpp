#include <iostream>
#include <ctime>
#include "calculateurscoretennis.h"

using namespace std;

void afficherScoreEnCours(CalculateurScoreTennis jeu)
{
    std::cout << jeu.RenvoyerScorePourAffichage() << std::endl;
}

int main()
{
    CalculateurScoreTennis jeu;
    afficherScoreEnCours(jeu);
    jeu.Joueur1AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur1AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur1AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur1AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);
    jeu.Joueur2AMarque();
    afficherScoreEnCours(jeu);

    jeu = CalculateurScoreTennis(-2,3);
    afficherScoreEnCours(jeu);
    return 0;
}
