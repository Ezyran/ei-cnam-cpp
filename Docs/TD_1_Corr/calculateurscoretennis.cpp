#include "calculateurscoretennis.h"
#include <string>
#include <math.h>

CalculateurScoreTennis::CalculateurScoreTennis():m_pointsMarquesJoueur1(),m_pointsMarquesJoueur2()
{
}

CalculateurScoreTennis::CalculateurScoreTennis(unsigned int pointsMarquesJoueur1, unsigned int pointsMarquesJoueur2)
    : m_pointsMarquesJoueur1(pointsMarquesJoueur1), m_pointsMarquesJoueur2(pointsMarquesJoueur2)
{    // Programmation défensive
    if (m_pointsMarquesJoueur1 < 0 || m_pointsMarquesJoueur2 < 0){
        // "Score négatif, mauvaises valeurs passées en paramètre";
        // Il faudrait lancer une exception ici,
        // ou utiliser des entiers non signés pour empêcher les négatifs par exemple
    }
}

std::string  CalculateurScoreTennis::RenvoyerScorePourAffichage()
{
    // A partir du moment où un joueur a marqué 4 échanges, les règles sont particulières
    if (JeuEnPhaseFinale())
    {
        return UnJoueurAMarque4Points();
    }

    // En cas d'égalité, on formate le score différemment.
    if (ScoreCourantEstEgalite()){
        return ConvertirNombreDePointsEnScore(m_pointsMarquesJoueur1) + " A";
    }

    // Cas nominal
    return PartieEnCours();
}

std::string CalculateurScoreTennis::ConvertirNombreDePointsEnScore(int pointsMarquesJoueur)
{
    if (pointsMarquesJoueur == 1)
        return  "15";
    if (pointsMarquesJoueur == 2)
        return  "30";
    if (pointsMarquesJoueur == 3)
        return  "40";
    return "0";
}

std::string CalculateurScoreTennis::UnJoueurAMarque4Points()
{
    if(abs(m_pointsMarquesJoueur1 - m_pointsMarquesJoueur2) >= 2) // => différence de 2 points minimum, quelqu'un a gagné
    {
        return UnJoueurAGagne();
    }
    else if (m_pointsMarquesJoueur1 == m_pointsMarquesJoueur2){ // égalité
        return "Egalite";
    }
    else{
        return AvantageJoueur();
    }
}

std::string CalculateurScoreTennis::UnJoueurAGagne()
{
    std::string JoueurGagnant;
    if (m_pointsMarquesJoueur1 > m_pointsMarquesJoueur2){
        JoueurGagnant = "1";
    }else{
        JoueurGagnant = "2";
    }
    return  "Le joueur "+ JoueurGagnant + " a gagne le jeu.";
}

std::string CalculateurScoreTennis::AvantageJoueur()
{
    std::string Avantage = "Avantage joueur ";
    if(m_pointsMarquesJoueur1 > m_pointsMarquesJoueur2)
    {
        Avantage += "1";
    }
    else {
        Avantage += "2";
    }
    Avantage += ". Balle de jeu.";
    return Avantage;
}

std::string CalculateurScoreTennis::PartieEnCours()
{
    std::string resultatScore = "";
    std::string resultatJoueur1, resultatJoueur2;

    resultatJoueur1 = ConvertirNombreDePointsEnScore(m_pointsMarquesJoueur1);
    resultatJoueur2 = ConvertirNombreDePointsEnScore(m_pointsMarquesJoueur2);

    resultatScore = resultatJoueur1 + " - " + resultatJoueur2;

    return resultatScore;
}
