#ifndef TENNISSCORE_H
#define TENNISSCORE_H

#include <string>


class CalculateurScoreTennis
{
public:
    // Constructeur par défaut.
    CalculateurScoreTennis();
    // Constructeur avec paramètres, l'utilisation d'entiers non signés empêche l'usage de nombre négatifs
    CalculateurScoreTennis(unsigned int pointsMarquesJoueur1, unsigned int pointsMarquesJoueur2);

    // Méthode publique qui calcule le score. Considérée comme un "usecase"
    std::string RenvoyerScorePourAffichage();

    // Méthodes publiques pour faire varier le score.
    // On ne veut pas que les score puissent être manipulés n'importe comment,
    inline void Joueur1AMarque(){m_pointsMarquesJoueur1++;}
    inline void Joueur2AMarque(){m_pointsMarquesJoueur2++;}

private:
    // Score de chaque joueur.
    int m_pointsMarquesJoueur1;
    int m_pointsMarquesJoueur2;

    // Fonction "de base"
    std::string ConvertirNombreDePointsEnScore(int pointsMarquesJoueur);

    // Fonctions pour effectuer les traitements selon l'état de la classe.
    std::string UnJoueurAMarque4Points();
    std::string UnJoueurAGagne();
    std::string AvantageJoueur();
    std::string PartieEnCours();

    // Fonctions pour exprimer les règles métier qui dirigent l'algo.
    inline bool  JeuEnPhaseFinale(){ return m_pointsMarquesJoueur1 >= 4 || m_pointsMarquesJoueur2 >= 4; }
    inline bool  ScoreCourantEstEgalite(){ return m_pointsMarquesJoueur1  > 0 && m_pointsMarquesJoueur1 == m_pointsMarquesJoueur2;}

};

#endif // TENNISSCORE_H
