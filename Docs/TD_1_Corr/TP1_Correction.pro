TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        calculateurscoretennis.cpp \
        main.cpp

HEADERS += \
    calculateurscoretennis.h
