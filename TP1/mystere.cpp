#include <iostream>


// L'ordinateur devine lui-même le nombre mystère.
// Utilise la méthode de la dichotomie.
void autoGuesser(int secret)
{
    int guess = -1;
    int min = 0;
    int max = 999;
    while (guess != secret)
    {
        guess = (min+max)/2;
        std::cout << "Essai = " << guess << " . ";
        if (guess < secret)
        {
            std::cout << "C'est plus grand !" << std::endl;
            min = guess;
        }
        else if (guess > secret)
        {
            std:: cout << "C'est plus petit !" << std::endl;
            max = guess;
        }
     }
     std::cout << "C'était bien " << secret << " !" << std::endl;
}

// Le joueur doit deviner lui-même le nombre mystère.
void playGuesser(int secret)
{
    int guess = -1;
    int trials = 0;
    while (guess != secret)
    {
        std::cout << "Devinez le nombre mystère ? :" << std::endl;
        std::cin >> guess;

        if (guess < secret)
            std::cout << "C'est plus grand !" << std::endl;
        else if (guess > secret)
            std::cout << "C'est plus petit !" << std::endl;
        else
            std::cout << "C'est ça !" << std::endl;
        trials++;        
    }

    std::cout << "La bonne réponse était : " << secret << std::endl;
    std::cout << "Tu as trouvé la réponse en " << trials << " essais." << std::endl; 
}

// Fonction principale de l'exercice III.2
int main(int argc, char const *argv[])
{
    int secret = rand() % 1000;

    std::cout << " ######## Jeu du nombre mystère ########" << std::endl;
    std::cout << "Choisissez :" << std::endl;
    std::cout << "[0] Jouer" << std::endl;
    std::cout << "[1] Faire jouer l'ordinateur" << std::endl;
    std::cout << "Entrez autre chose pour quitter ..." << std::endl;

    int choix;
    std::cin >> choix;

    if (choix == 0)
        playGuesser(secret);
    else if (choix == 1)
        autoGuesser(secret);

    return 0;
}
