#include <iostream>
#include <cmath>

// I.1.1 : Renvoie la somme de deux entiers passés en paramètre.
int sum(int a, int b) 
{
    return a + b;
}

// I.1.2 : Inverse les valeurs de deux entiers passés en paramètre.
void flip(int* a, int* b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

// I.1.3.a : Prend 3 paramètres, et remplace le 3ème par la somme des deux autres.
// Version avec pointeurs
void sum_replace_ptr(int a, int b, int *c) 
{
    *c = a + b;
}

// I.1.3.b : Prend 3 paramètres, et remplace le 3ème par la somme des deux autres.
// Version avec références
void sum_replace_ref(int a, int b, int &c) 
{
    c = a + b;
}

// I.1.4 : Tableau aléatoire et tris

// Affichage un tableau de taille size.
void printTab(int* tab, int size)
{
    for(int i = 0; i < size; i++)
        std::cout << i << " : " << tab[i] << std::endl;
}

// Remplis un tableau de taille size de valeurs aléatoires inférieures à 100.
// Bonus 1
void genRandomTab(int* tab, int size)
{
    for(int i = 0; i < size; i++)
        tab[i] = std::rand() % 100;
}

// Tri croissant d'un tableau de taille size (Tri par sélection)
// Bonus 1
void ascendingSort(int* tab, int size)
{
    for(int i = 0; i < size-1; i++)
    {
        int min = i;
        for(int j = i+1; j < size; j++)
        {
            if (tab[j] < tab[min])
                min = j;
        }
        if (min != i)
            flip(&tab[i], &tab[min]);
    }
}

// Tri décroissant d'un tableau de taille size (Tri par sélection)
// Bonus 1
void descendingSort(int* tab, int size)
{
    for(int i = 0; i < size-1; i++)
    {
        int min = i;
        for(int j = i+1; j < size; j++)
        {
            if (tab[j] > tab[min])
                min = j;
        }
        if (min != i)
            flip(&tab[i], &tab[min]);
    }
}

// Inverse un tableau donné en entrée
// Bonus 2
void flipTable(int* tab, int size)
{
    for (int i = 0; i < std::floor(size/2); i++)
    {
        flip((tab+i), (tab+(size-1)-i));
    }
}

// Fonction principale de l'exercice I.1.4
int random_tab()
{
    int n = 0;
    std::cout << "Entrez la taille du tableau aléatoire à générer : " << std::endl;
    std::cin >> n;
    
    // Définition et affichage du tableau
    int tab[n];
    std::cout << "## Tableau aléatoire non-trié :" << std::endl;
    genRandomTab(tab, n);
    printTab(tab, n);

    int sortType = 0;
    std::cout << "Entrez 0 pour un tri croissant, 1 pour un tri décroissant : " << std::endl;
    std::cin >> sortType;
    
    if (sortType == 0)
        ascendingSort(tab,n);
    else if (sortType == 1)
        descendingSort(tab,n);
    else
    {
        std::cout << "Hého, c'est quoi ça." << std::endl;
        return -1;
    }
    
    // Affichage du tableau trié
    std::cout << " ## Tableau aléatoire trié :" << std::endl;
    printTab(tab, n);

    std::cout << " ## Tableau aléatoire trié inversé " << std::endl;
    flipTable(tab, n);
    printTab(tab, n);

    return 0;
}

// Programme principal de l'exercice I
int main(int argc, char const *argv[])
{
    int exo = 0;
    std::cout << "Choisissez un exercice :" << std::endl;
    std::cout << "[1] I.1.1  : Somme d'entiers" << std::endl;
    std::cout << "[2] I.1.2  : Inversion de valeurs" << std::endl;
    std::cout << "[3] I.1.3a : Remplacement de valeur avec pointeurs" << std::endl;
    std::cout << "[4] I.1.3b : Remplacement de valeur avec référence" << std::endl;    
    std::cout << "[5] I.1.4  : Tableau aléatoire" << std::endl;
    std::cout << "Sélection ? [1,2,3,4,5] (Autre chose pour quitter):" << std::endl;
    std::cin >> exo;

    if (exo == 5)
        random_tab();
    else if (exo <= 4)
    {
        int a, b, c;
        std::cout << "Entrez une valeur de A :" << std::endl;
        std::cin >> a;
        std::cout << "Entrez une valeur de B :" << std::endl;
        std::cin >> b;
        std::cout << "Entrez une valeur de C :" << std::endl;
        std::cin >> c;
        std::cout << "A=" << a << ";B=" << b << ";C=" << c << ";" << std::endl;

        switch (exo)
        {
        case 1:
            std::cout << "A+B = " << sum(a,b) << std::endl;
            break;
        
        case 2:
            std::cout << "## Inversion de A et de B :" << std::endl;
            std::cout << "Avant inversion : A=" << a << ", B=" << b << std::endl;
            flip(&a, &b);
            std::cout << "Après inversion : A=" << a << ", B=" << b << std::endl;
            break;
            
        case 3:
            std::cout << "## Remplacement de C par A+B (via pointeur) :" << std::endl;
            std::cout << "Avant : A=" << a << ";B=" << b << ";C=" << c << ";" << std::endl;
            sum_replace_ptr(a, b, &c);
            std::cout << "Après : A=" << a << ";B=" << b << ";C=" << c << ";" << std::endl;
            break;

        case 4:
            std::cout << "## Remplacement de C par A+B (via référence) :" << std::endl;
            int& ref_c = c;
            std::cout << "Avant : A=" << a << ";B=" << b << ";C=" << c << ";" << std::endl;
            sum_replace_ref(a, b, ref_c);
            std::cout << "Après : A=" << a << ";B=" << b << ";C=" << c << ";" << std::endl;
            break;
        }
    }

    return 0;
}

