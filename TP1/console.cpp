#include <iostream>
#include <string>

int main(int argc, char const *argv[])
{
    std::string entree, nom, prenom;

    std::cout << "Coucou. Comment t'appelles-tu ? (Nom Prénom, stp)" << std::endl;
    std::getline(std::cin, entree);

    // Recherche du premier espace dans la chaîne entree
    int separateur = 0;
    int i = 0;
    while (i < entree.length() && separateur == 0)
    {
        if (entree[i] == ' ')
            separateur = i;
        i++;
    }

    // Séparation du nom et du prénom depuis l'entrée
    nom = entree.substr(0, separateur);
    prenom = entree.substr(separateur+1);

    // Mise en majsucule du premier charactère du prénom
    prenom[0] = std::toupper(prenom[0]);

    // Mise en majuscule du nom
    for (int j = 0; j < nom.length(); j++)
        nom[j] = std::toupper(nom[j]);

    // Affichage du résultat
    std::cout << "Bonjour, " << prenom << " " << nom << " !" << std::endl;

    return 0;
}
